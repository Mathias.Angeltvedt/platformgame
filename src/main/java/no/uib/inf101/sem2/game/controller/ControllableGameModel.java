package no.uib.inf101.sem2.game.controller;

public interface ControllableGameModel {
    boolean movePlayer(double deltaX, double deltaY);
}
