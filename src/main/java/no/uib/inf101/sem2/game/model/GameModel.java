package no.uib.inf101.sem2.game.model;

import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.sem2.game.controller.ControllableGameModel;
import no.uib.inf101.sem2.game.model.Entities.EntityFactory;
import no.uib.inf101.sem2.game.model.Entities.Player;
import no.uib.inf101.sem2.game.view.ViewableGameModel;

public class GameModel implements ViewableGameModel, ControllableGameModel{
    Player player;
    EntityFactory entityFactory;

    public GameModel(EntityFactory entityFactory) {
        this.entityFactory = entityFactory;
        player = entityFactory.newPlayer();
    }

    @Override
    public boolean movePlayer(double deltaX, double deltaY) {
        Player potentialPlayerPos = player.shiftedBy(deltaX, deltaY);
        if (true) {
            this.player = potentialPlayerPos;
            return true;
        }
        return false;
    }

    @Override
    public List<Double> getPlayerXAndYPos() {
        List<Double> playerPositions = new ArrayList<>();
        playerPositions.add(player.getXPos());
        playerPositions.add(player.getYPos());
        return playerPositions;
    }
    
}
