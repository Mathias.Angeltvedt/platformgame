package no.uib.inf101.sem2.game.view;

import java.util.List;

public interface ViewableGameModel {
    List<Double> getPlayerXAndYPos();
}
