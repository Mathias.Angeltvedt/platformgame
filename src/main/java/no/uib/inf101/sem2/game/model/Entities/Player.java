package no.uib.inf101.sem2.game.model.Entities;

public class Player {
    private double xPos;
    private double yPos;


    Player(double xPos, double yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public Player shiftedBy(double xPos, double yPos) {
        return new Player(this.xPos + xPos, this.yPos + yPos);
    }

    public double getXPos() {
        return xPos;
    }

    public double getYPos() {
        return yPos;
    }
}
