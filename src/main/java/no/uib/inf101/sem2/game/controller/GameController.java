package no.uib.inf101.sem2.game.controller;

import java.awt.event.KeyEvent;

import javax.swing.JPanel;

import no.uib.inf101.sem2.game.model.GameModel;
import no.uib.inf101.sem2.game.view.GameView;

public class GameController extends JPanel implements java.awt.event.KeyListener {
    GameModel gameModel;
    GameView gameView;

    public GameController(GameModel gameModel, GameView gameView) {
        this.gameModel = gameModel;
        this.gameView = gameView;
        gameView.setFocusable(true);
        gameView.addKeyListener(this);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W -> gameModel.movePlayer(0.0, -2.0);
            case KeyEvent.VK_A -> gameModel.movePlayer(-2.0, 0.0);
            case KeyEvent.VK_S -> gameModel.movePlayer(0.0, 2.0);
            case KeyEvent.VK_D -> gameModel.movePlayer(2.0, 0.0);
        }
        gameView.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }
    
}
