package no.uib.inf101.sem2.game.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import no.uib.inf101.sem2.game.model.GameModel;

public class GameView extends JPanel {
    private final int SCREENHEIGHT = 960;
    private final int SCREENWIDTH = 640;
    private final Dimension DIMENSION = new Dimension(SCREENWIDTH, SCREENHEIGHT);
    private BufferedImage spriteImage;
    private GameModel gameModel;

    public GameView(GameModel gameModel) {
        this.gameModel = gameModel;
        this.spriteImage = Inf101Graphics.loadImageFromResources("/game_sprite.png");
        this.setFocusable(true);
        this.setMinimumSize(DIMENSION);
        this.setPreferredSize(DIMENSION);
        this.setMaximumSize(DIMENSION);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Inf101Graphics.drawImage(g, spriteImage.getSubimage(0, 0, 32, 32), gameModel.getPlayerXAndYPos().get(0), gameModel.getPlayerXAndYPos().get(1), 2);
    }

    
}
