package no.uib.inf101.sem2.game;

import no.uib.inf101.sem2.game.controller.GameController;
import no.uib.inf101.sem2.game.model.GameModel;
import no.uib.inf101.sem2.game.model.Entities.EntityFactory;
import no.uib.inf101.sem2.game.view.GameView;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    EntityFactory entityFactory = new EntityFactory();
    GameModel gameModel = new GameModel(entityFactory);
    GameView gameView = new GameView(gameModel);
    new GameController(gameModel, gameView);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Platform Game");
    frame.setContentPane(gameView);
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setResizable(false);
    frame.setVisible(true);
  }
}
