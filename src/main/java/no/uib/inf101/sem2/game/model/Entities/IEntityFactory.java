package no.uib.inf101.sem2.game.model.Entities;

public interface IEntityFactory {
    Player newPlayer();
}
